package u07asg

import u07asg.Scala2P._
import alice._
import alice.tuprolog.{Struct, Term, Theory}
import java.io.FileInputStream
import java.util.Optional
import java.util
import scala.collection.mutable.Buffer
import collection.JavaConverters._


class TicTacToeImpl(fileName: String) extends TicTacToe {

  implicit private def playerToString(player: Player): String = player match {
    case Player.PlayerGREEN => "p1"
    case _ => "p2"
  }
  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.PlayerGREEN
    case _ => Player.PlayerRED
  }

  implicit def listToTerm[T](l: List[T]): Term = l.mkString("[",",","]")

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  createBoard()

  override def createBoard() = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess(engine,goal)
  }

  override def getBoard() = {
    val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map{
        case "null" => Optional.empty[Player]()
        case s => Optional.of[Player](s)
      }.to[Buffer].asJava
  }

      //controllo con prolog se la tabella è stata completata, quindi tutti le celle sono occupate ma nessuno ha fatto tris
  override def checkCompleted() = {
    val goal = "board(B),final(B,_)"
    solveWithSuccess(engine, goal)
  }

  //se è stato completata la partita devo controllare chi ha ha vinto, con prolog controllo se è stata completato il tris e chi ha vinto
  /*override def checkVictory(X : Int, Y : Int) = {
    val goal = "board(B),final(B,p1,X,Y)"
    val goal2 = "board(B),final(B,p2)"
    if (solveWithSuccess(engine, goal)) Optional.of(Player.PlayerX)
    else if (solveWithSuccess(engine, goal2)) Optional.of(Player.PlayerO)
    else Optional.empty()
  }*/
  override def checkVictory(scoresP1: util.List[_], scoresP2: util.List[_]): Optional[Player] ={
    val goal = "resultPlayer(" + scoresP1 + ",32)"
    val goal2 = "resultPlayer(" + scoresP2 + ",32)"
    if (solveWithSuccess(engine, goal)) Optional.of(Player.PlayerGREEN)
    else if (solveWithSuccess(engine, goal2)) Optional.of(Player.PlayerRED)
    else Optional.empty()
  }

  override def move(player: Player, i: Int, j: Int): Boolean = {
    val goal = s"board(B), next_board(B,${playerToString(player)},B2)"
    val nextboard = (for {
      term <- engine(goal).map(extractTerm(_, "B2"))          //controlla che ci sia stata una mossa del player come nuova tavola(board)
      elem = term.asInstanceOf[Struct].listIterator().asScala.toList(i + 4 * j)
      if (elem.toString == playerToString(player))
    } yield term).headOption
    if (nextboard isEmpty) return false
    val goal2 = s"retractall(board(_)), assert(board(${nextboard.get.toString}))"
    solveWithSuccess(engine,goal2)
  }

  override def toString =
    solveOneAndGetTerm(engine,"board(B)","B").toString

  override def winCount(current: Player, winner: Player): Int = {
    val goal = s"board(B), statistics(B,${playerToString(current)},${playerToString(winner)},Count)"
    solveOneAndGetTerm(engine,goal,"Count").asInstanceOf[tuprolog.Int].intValue()
  }
}
