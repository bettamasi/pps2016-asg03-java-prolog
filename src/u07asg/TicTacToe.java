package u07asg;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

enum Player { PlayerGREEN, PlayerRED }

public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory(List scoresP1, List scoresP2);

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);
}
