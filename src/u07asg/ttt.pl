
% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player GREEN wins").
result(p2,"player RED wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(16,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

%checks if the player has reached his goal: get 30points
resultPlayer([],0).
resultPlayer([Head|Tail],ScoreGoal):- resultPlayer(Tail,NewTail), ScoreGoal is NewTail + Head.


% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).
