package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[4][4];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerGREEN;
    private int scoreButton = 0, scorePlayer1, scorePlayer2;
    private final ArrayList scoresP1 = new ArrayList();
    private final ArrayList scoresP2 = new ArrayList();

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerGREEN ? Player.PlayerRED : Player.PlayerGREEN;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }

    private void humanMove(int i, int j){
        //se c'è stata una mossa colora in base al giocatore
        if (ttt.move(turn,i,j)){
            if(turn == Player.PlayerGREEN){
                scorePlayer1 = Integer.parseInt(board[i][j].getText());
                scoresP1.add(scorePlayer1);
                board[i][j].setForeground(Color.GREEN);
            }else{
                scorePlayer2 = Integer.parseInt(board[i][j].getText());
                scoresP2.add(scorePlayer2);
                board[i][j].setForeground(Color.RED);}
            changeTurn();
        }
        Optional<Player> victory = ttt.checkVictory(scoresP1, scoresP2);
        if (victory.isPresent()){
            exit.setText(victory.get()+" won!");
            finished=true;
            return;
        }
        if (ttt.checkCompleted()){
            exit.setText("Even!");
            finished=true;
            return;
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(4,4));

        for (int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                final int i2 = i;
                final int j2 = j;
                scoreButton++;
                board[i][j]=new JButton(String.valueOf(scoreButton));
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,230);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}